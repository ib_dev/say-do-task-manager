# Say & Do - Task Manager

## Overview ##

Task management software with mostly real-time UI and database which is self-updating.  
Projects associated with tasks. Sorting and dragging tasks between three groups. Time counting for active tasks.  
Registration with email validation, password reset, responsive design, etc.

## Test it ##

[Demo](https://fir-todo-list-da99d.firebaseapp.com)

## The most important technologies used ##

React, Redux, Firebase, CSS Modules, Sass, Babel, Semantic UI React, React Router.

## Setup ##

Install [Node.js](http://nodejs.org/). This will also install npm.    
Run `npm install` to install the dependencies of this project.   
Run `npm start` to start a localhost and open it in the default browser.   

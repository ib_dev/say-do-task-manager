import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import UserRoute from './../common/components/routes/UserRoute';

import ProjectPage from './components/pages/project/ProjectPage';
import NewProjectPage from './components/pages/project/NewProjectPage';
import TasksPage from './components/pages/task/TasksPage';
import UpdateProjectPage from './components/pages/project/UpdateProjectPage';
import NotFoundPage from './../common/components/pages/NotFoundPage';
import { wrapper } from './index.css';

const TodoList = ({location, match}) => {
  return (
    <div className={wrapper}>
      <Switch>
        <UserRoute path={match.url + "/projects"} exact component={ProjectPage} location={location}/>
        <UserRoute path={match.url + "/projects/add-new"} exact component={NewProjectPage} location={location}/>
        <UserRoute path={match.url + "/projects/update/:projectId"} exact component={UpdateProjectPage} location={location}/>
        <UserRoute path={match.url + "/tasks/:projectId"} exact component={TasksPage} location={location}/>
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
}

TodoList.propTypes = {
  location: PropTypes.shape({
    pathname:  PropTypes.string.isRequired
  }).isRequired
}

export default TodoList;

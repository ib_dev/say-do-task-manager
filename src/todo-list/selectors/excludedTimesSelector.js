import { createSelector }  from 'reselect';
import createExcludedTimes from './../utils/createExcludedTimes';

const getTasks = state => state.tasks;
const getTimestamp = state => state.timestamp;

// Creates object consisting task id's as keys and
// exluded times (for datepicker) as value
const createList = (tasks, timestamp) => {
  return Object.entries(tasks).reduce( (accumulator, currentValue) => {
    const [key, val] = currentValue;
    if (val.deadline) {
      const time = {
        [key]: createExcludedTimes(val.deadline, timestamp)
      }
      return { ...accumulator, ...time}
    } else {
      const time = {
        [key]: createExcludedTimes(timestamp, timestamp)
      }
      return { ...accumulator, ...time}
    }
  }, {})
}

export const excludedTimesSelector = createSelector(getTasks, getTimestamp, createList);

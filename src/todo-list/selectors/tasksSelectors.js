import { createSelector }  from 'reselect';

const getActiveOrder = state => state.tasksOrders.activeOrder;
const getCompletedOrder = state => state.tasksOrders.completedOrder;
const getPendingOrder = state => state.tasksOrders.pendingOrder;
const getTasks = state => state.tasks;

const createList = (orderA, tasks, orderB, orderC) => {
  if (orderA === null || orderB === null || orderC === null) {
    return null
  } else if ( (orderA.length + orderB.length + orderC.length) === Object.keys(tasks).length ) {
    return orderA.map(key => {
      let task = {...tasks[key]};
      task.id = key;
      const taskLength = task.content.trim().length
      if (taskLength < 3 || taskLength > 2000) {
        task.error = true;
      } else {
        task.error = false;
      }
      return task;
    })
  } else {
    return null
  }
}

export const activeOrderSelector =
  createSelector(getActiveOrder, getTasks, getCompletedOrder, getPendingOrder, createList);
export const completedOrderSelector =
  createSelector(getCompletedOrder, getTasks, getActiveOrder, getPendingOrder, createList);
export const pendingOrderSelector =
  createSelector(getPendingOrder, getTasks, getActiveOrder, getCompletedOrder, createList);

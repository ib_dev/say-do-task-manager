import { createSelector }  from 'reselect';

// selector
const getProjects = state => state.projects.list;

// for single and multible projects
export const projectsSelector = createSelector(getProjects, projects => {
  return Object.entries(projects).map(([key, val])=> {
    let project = {...val};
    project.id = key;
    return project;
  });
});

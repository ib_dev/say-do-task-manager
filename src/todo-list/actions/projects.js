import api from './../../api';
import {
  PROJECT_ADDED,
  PROJECT_REMOVED,
  PROJECT_UPDATED,
  REMOVE_ALL_LISTENERS,
  PROJECTS_LISTENER,
  PROJECTS_REQUESTED
} from './../../types';

export const projectAdded = project => ({
  type: PROJECT_ADDED, project
});
export const projectRemoved = key => ({
  type: PROJECT_REMOVED, key
});
export const projectUpdated = project => ({
  type: PROJECT_UPDATED, project
});

export const add = (uid, pr) => () => api.project.add(uid, pr)
export const remove = (uid, key) => () => api.project.remove(uid, key)

export const removeAllListeners = () => dispatch => {
  dispatch({ type: REMOVE_ALL_LISTENERS })
}


export const addProjectListeners = uid => dispatch => {
  api.project.fetchAll(uid).then(({data, lastCreatedAt, listenerRef}) => {
    dispatch({ type: PROJECTS_REQUESTED });
    dispatch({ type: PROJECTS_LISTENER, listenerRef });

    if (data) dispatch(projectAdded(data));

    api.project.projectAdded(uid, lastCreatedAt, pr => {
      dispatch(projectAdded(pr));
    })
  })

  api.project.projectRemoved(uid, key => {
    dispatch(projectRemoved(key));
  })

  api.project.projectChanged(uid, pr => {
    dispatch(projectUpdated(pr))
  })
}

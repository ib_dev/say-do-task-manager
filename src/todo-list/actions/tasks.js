import {
  REMOVE_ALL_LISTENERS,
  REMOVE_ALL_TASKS,
  REMOVE_TASKS_ORDERS,
  LISTEN_TASKS_ORDER,
  LISTEN_TASKS,
  UPDATE_TASK,
  REORDER_TASKS,
  REMOVE_TASK
} from './../../types';


export const addTaskListeners = () => dispatch => {
  dispatch({
    type: LISTEN_TASKS_ORDER
  });
  dispatch({
    type: LISTEN_TASKS
  });
}

export const cleanTasksState = () => dispatch => {
  dispatch({
    type: REMOVE_ALL_LISTENERS
  });
  dispatch({
    type: REMOVE_ALL_TASKS
  });
  dispatch({
    type: REMOVE_TASKS_ORDERS
  });
}

export const updateTaskAction = (key, content) => ({
  type: UPDATE_TASK, key, content
});

export const updateTask = (key, content) => dispatch => {
  dispatch(updateTaskAction(key, content));
}


export const reorderTasks = groupes => dispatch => {
  dispatch({
    type: REORDER_TASKS,
    groupes
  });
}

export const removeTask = (key, taskType) => dispatch => {
  dispatch({
    type: REMOVE_TASK,
    key, taskType
  });
}

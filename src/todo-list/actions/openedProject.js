import { PROJECT_OPENED, PROJECT_CLOSED } from './../../types';

export const projectOpened = id => ({
  type: PROJECT_OPENED,
  id
});
export const setOpenedProject = id => dispatch => {
  dispatch(projectOpened(id));
}

export const projectClosed = () => ({
  type: PROJECT_CLOSED
});
export const removeOpenedProject = id => dispatch => {
  dispatch(projectClosed());
}

import fd from 'format-date';

const formatDate = timestamp => {
  return fd('{month-name} {day}, {year} at {hours}:{minutes}', new Date(timestamp));
}

export default formatDate

import moment from 'moment';

const getDeadline = deadline => deadline ? moment(deadline) : null;

export default getDeadline;

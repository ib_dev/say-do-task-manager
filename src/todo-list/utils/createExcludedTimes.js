import moment from 'moment';

// Adds full hour's into array from beginning of day to selected time
const createExcludedTimes = (selected, current) => {
  const selectedStartOfDay = moment(selected).startOf('day').valueOf()
  const currentStartOfDay = moment(current).startOf('day').valueOf()

  if (selectedStartOfDay === currentStartOfDay) {
    let excluded = current
    let h = 0
    const times = []
    while (excluded <= current) {
      let time = moment(current).startOf('day').add(h, 'hours');
      times.push(time)
      excluded = time.valueOf()
      h++;
    }
    return times.slice(0, -1)
  } else {
    return []
  }
}

export default createExcludedTimes

import api from './../../api';
import { db } from './../../fb';
import { UPDATE_TASK } from './../../types';

const updateTask = store => next => action => {
  switch (action.type){
    case UPDATE_TASK:
      const uid = store.getState().user.uid;
      const projectId = store.getState().openedProject;
      const tasksRef = db.ref(`todo-list/${uid}/lists/${projectId}/tasks`)
      api.tasks.update(tasksRef.child(action.key), action.content)
      break;
    default:
      next(action);
  }
}

export default updateTask;

import api from './../../api';
import { db } from './../../fb';
import {
  NEW_ACTIVE_ORDER,
  NEW_ACTIVE_ORDER_LISTENER,
  LISTEN_TASKS_ORDER,
  REORDER_TASKS,
  NEW_COMPLETED_ORDER,
  NEW_COMPLETED_ORDER_LISTENER,
  NEW_PENDING_ORDER_LISTENER,
  NEW_PENDING_ORDER
} from './../../types';
import cfl from './../../common/utils/capitalizeFirstLetter';
import makeActionCreator from './../../common/utils/makeActionCreator';
import { updateTaskAction } from './../actions/tasks';


const updateExtras = ({id, task, type, timestamp, dispatch}) => {
  if (!task.activeAt && type === 'active') {
    dispatch(updateTaskAction(id, {activeAt: timestamp}))
  } else if (task.activeAt && type !== 'active') {

    const activeTime =
      task.activeTime ?
      (task.activeTime + timestamp - task.activeAt) :
      timestamp - task.activeAt;

    const extras = {
      activeTime,
      activeAt: null,
      completedAt: type === 'completed' ? timestamp : null
    }

    dispatch(updateTaskAction(id, extras))
  }
}


export const aTree = {
  newActiveOrder: makeActionCreator(NEW_ACTIVE_ORDER, 'order'),
  newCompletedOrder: makeActionCreator(NEW_COMPLETED_ORDER, 'order'),
  newPendingOrder: makeActionCreator(NEW_PENDING_ORDER, 'order'),
  newActiveOrderListener: makeActionCreator(NEW_ACTIVE_ORDER_LISTENER, 'listenerRef'),
  newCompletedOrderListener: makeActionCreator(NEW_COMPLETED_ORDER_LISTENER, 'listenerRef'),
  newPendingOrderListener: makeActionCreator(NEW_PENDING_ORDER_LISTENER, 'listenerRef')
}

export const activeOrder = store => next => action => {
  const state = store.getState();
  const uid = state.user.uid;
  const projectId = state.openedProject;
  const projectRef = db.ref(`todo-list/${uid}/lists/${projectId}`);
  const orderTypes = Object.keys(state.tasksOrders)

  switch (action.type){
    case LISTEN_TASKS_ORDER:
      orderTypes.forEach(type => {
        const orderRef = projectRef.child(type)
        store.dispatch( aTree[`new${cfl(type)}Listener`](orderRef) )
        api.tasks.newOrder(orderRef, snapVal => {
          let order = []
          if (snapVal) order = snapVal.split(' ')
          store.dispatch(aTree[`new${cfl(type)}`](order))
        })
      });
      break;
    case REORDER_TASKS:
      const { tasks, timestamp } = state;

      action.groupes.forEach(group => {
        group.order.forEach(id => {
          updateExtras({
            id,
            timestamp,
            task: tasks[id],
            type: group.tasksType,
            dispatch: store.dispatch
          })
        })
        // updates order
        store.dispatch( aTree[`new${cfl(group.tasksType)}Order`](group.order) )
        const orderRef = projectRef.child(`${group.tasksType}Order`)
        api.tasks.updateOrder(orderRef, group.order);
      })
      break;
    default:
      next(action);
  }

}

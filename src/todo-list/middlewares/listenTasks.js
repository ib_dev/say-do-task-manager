import api from './../../api';
import { db } from './../../fb';
import {
  LISTEN_TASKS,
  TASK_ADDED,
  TASKS_LISTENER,
  TASK_REMOVED,
  TASK_UPDATED
} from './../../types';
import makeActionCreator from './../../common/utils/makeActionCreator';

export const tasksListener = makeActionCreator(TASKS_LISTENER, 'tasksRef')
export const taskAdded = makeActionCreator(TASK_ADDED, 'task')
export const taskRemoved = makeActionCreator(TASK_REMOVED, 'key')

export const taskUpdated = task => dispatch => {
  dispatch({
    type: TASK_UPDATED,
    task
  });
}

const listenTasks = store => next => action => {
  switch (action.type){
    case LISTEN_TASKS:
      const uid = store.getState().user.uid;
      const projectId = store.getState().openedProject;
      const tasksRef = db.ref(`todo-list/${uid}/lists/${projectId}/tasks`)

      store.dispatch(tasksListener(tasksRef))

      api.tasks.fetchAll(tasksRef).then(({data, lastCreatedAt}) => {
        if (data) store.dispatch(taskAdded(data))
        api.tasks.taskAdded(tasksRef, lastCreatedAt, newTask => {
          store.dispatch(taskAdded(newTask))
        })
      })

      api.tasks.taskRemoved(tasksRef, key => {
        store.dispatch(taskRemoved(key));
      })

      api.tasks.taskChanged(tasksRef, task => {
        store.dispatch(taskUpdated(task));
      })

      break;
    default:
      next(action);
  }
}

export default listenTasks

import api from './../../api';
import { db } from './../../fb';
import { REMOVE_TASK } from './../../types';

const removeTask = store => next => action => {
  switch (action.type){
    case REMOVE_TASK:
      const uid = store.getState().user.uid;
      const projectId = store.getState().openedProject;
      const projectRef = db.ref(`todo-list/${uid}/lists/${projectId}`)

      const tasksRef = projectRef.child('tasks')
      api.tasks.remove(tasksRef.child(`${action.key}`));

      const orderRef = projectRef.child(`${action.taskType}Order`);
      const order = store.getState().tasksOrders[`${action.taskType}Order`];
      const newOrder = order.filter(id => id !== action.key);
      api.tasks.updateOrder(orderRef, newOrder)
      break;
    default:
      next(action);
  }
}

export default removeTask;

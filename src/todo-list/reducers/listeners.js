import {
  NEW_ACTIVE_ORDER_LISTENER,
  TASKS_LISTENER,
  REMOVE_ALL_LISTENERS,
  NEW_COMPLETED_ORDER_LISTENER,
  PROJECTS_LISTENER
} from './../../types';

const initialState = {
  activeOrder: {},
  completedOrder: {},
  taskAdded: {},
  projectAdded: {}
}

export default function(state = initialState, action = {}) {
  switch (action.type){
    case PROJECTS_LISTENER:
      return {...state, projectAdded: action.listenerRef }
    case NEW_ACTIVE_ORDER_LISTENER:
      return {...state, activeOrder: action.listenerRef }
    case NEW_COMPLETED_ORDER_LISTENER:
      return {...state, completedOrder: action.listenerRef }
    case TASKS_LISTENER:
      return {...state, taskAdded: action.tasksRef }
    case REMOVE_ALL_LISTENERS:
      Object.entries(state).forEach(([key, val]) => {
        if (Object.keys(val).length) state[key].off()
      });
      return {...initialState};
    default:
      return state;
  }
}

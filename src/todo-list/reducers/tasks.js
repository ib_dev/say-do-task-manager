import omit from 'lodash/omit';
import {
  TASK_ADDED,
  TASK_REMOVED,
  REMOVE_ALL_TASKS,
  TASK_UPDATED
} from './../../types';

export default function(state = {}, action = {}) {
  switch (action.type){
    case TASK_ADDED:
    case TASK_UPDATED:
      return {...state, ...action.task}
    case TASK_REMOVED:
      return omit(state, action.key)
    case REMOVE_ALL_TASKS:
      return {}
    default:
      return state;
  }
}

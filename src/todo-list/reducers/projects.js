import omit from 'lodash/omit';
import {
  PROJECT_ADDED,
  PROJECT_REMOVED,
  PROJECT_UPDATED,
  USER_LOGGED_OUT,
  PROJECTS_REQUESTED
} from "./../../types";

const initialState = {
  list: {},
  loading: true
}

export default function(state = initialState, action = {}) {
  switch (action.type){
    case PROJECTS_REQUESTED:
      return {...state, loading: false }
    case PROJECT_ADDED:
    case PROJECT_UPDATED:
      return {...state, list: {...state.list, ...action.project}, loading: false }
    case PROJECT_REMOVED:
      return {...state, list: omit(state.list, action.key)}
    case USER_LOGGED_OUT:
      return {...initialState}
    default:
      return state;
  }
}

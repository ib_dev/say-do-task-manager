import {
  REMOVE_TASKS_ORDERS,
  NEW_ACTIVE_ORDER,
  NEW_COMPLETED_ORDER,
  NEW_PENDING_ORDER
} from './../../types';

const initialState = {
  activeOrder: null,
  completedOrder: null,
  pendingOrder: null
}

export default function(state = initialState, action = {}) {
  switch (action.type){
    case NEW_ACTIVE_ORDER:
      return {...state, activeOrder: action.order }
    case NEW_COMPLETED_ORDER:
      return {...state, completedOrder: action.order }
    case NEW_PENDING_ORDER:
      return {...state, pendingOrder: action.order }
    case REMOVE_TASKS_ORDERS:
      return {...initialState}
    default:
      return state;
  }
}

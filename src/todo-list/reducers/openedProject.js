import { PROJECT_OPENED, PROJECT_CLOSED } from './../../types';

export default function(state = '', action = {}) {
  switch (action.type){
    case PROJECT_OPENED:
      return action.id;
    case PROJECT_CLOSED:
      return '';
    default:
      return state;
  }
}

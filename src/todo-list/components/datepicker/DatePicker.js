import 'react-datepicker/dist/react-datepicker.css';
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import ReactDatepicker from 'react-datepicker';
import TaskDateInput from './TaskDateInput';
import getDeadline from './../../utils/getDeadline';
import { datepickerPopper } from './datepicker.css';


const DatePicker = ({timestamp, task, type, handleDateChange, excludedTimes}) => {
  return (
    <ReactDatepicker
      placeholderText="Select a date"
      todayButton={'Today'}
      customInput={<TaskDateInput timestamp={timestamp} />}
      selected={getDeadline(task.deadline)}
      onChange={handleDateChange.bind(null, task.id, type)}
      showTimeSelect
      dateFormat='YYYY-MM-DD HH:mm:ss'
      timeFormat="HH:mm"
      timeIntervals={60}
      minDate={moment(timestamp)} // current date
      excludeTimes={excludedTimes[task.id]}
      popperPlacement="left-end"
      popperClassName={datepickerPopper}
    />
  );
}

DatePicker.propTypes = {
  timestamp: PropTypes.number.isRequired,
  task: PropTypes.shape({
     content: PropTypes.string.isRequired,
     createdAt: PropTypes.number.isRequired,
     deadline: PropTypes.number,
     error: PropTypes.bool.isRequired,
     id: PropTypes.string.isRequired
  }).isRequired,
  type: PropTypes.string.isRequired,
  handleDateChange: PropTypes.func.isRequired,
  excludedTimes: PropTypes.objectOf(PropTypes.array.isRequired).isRequired
};

export default DatePicker;

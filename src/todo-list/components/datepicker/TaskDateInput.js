import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classNames from 'classnames';
import { Button, Icon } from 'semantic-ui-react'
import { deadlineOver, deadlineDay } from './datepicker.css';


class TaskDateInput extends React.Component {
  state = {
    current: null,
    selected: NaN
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { timestamp, value } = nextProps
    const selected = moment(value, "YYYY-MM-DD HH:mm:ss").valueOf()
    return (timestamp !== prevState.current) || (selected !== prevState.selected) ?
      {current: timestamp, selected} : null
  }

  deadlineOver = () => moment(this.state.selected).isBefore(this.state.current)
  timeToDeadline = () => moment(this.state.selected).to(moment(this.state.current), true)
  deadlineDay = () => moment(this.state.selected).isSame(this.state.current, 'day')
  deadlineSelected = () => !isNaN(this.state.selected)

  render() {
    const colorizeExpiry = classNames({
      icon: true,
      [deadlineOver]: this.deadlineOver(),
      [deadlineDay]: !this.deadlineOver() && this.deadlineDay()
    });

    return (
     <Button onClick={this.props.onClick} className={colorizeExpiry}>
       {!this.deadlineSelected() && <Icon name='calendar' />}
       {( this.deadlineSelected() && !this.deadlineOver() ) && this.timeToDeadline()}
       {this.deadlineOver() && 'Over'}
     </Button>
    );
  }

}

TaskDateInput.propTypes = {
  onClick: PropTypes.func,
  value: PropTypes.string,
  timestamp: PropTypes.number.isRequired
};

export default TaskDateInput;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { add } from './../../../actions/projects';
import ProjectForm from './../../forms/ProjectForm';
import { mainHeading } from './../pages.css';

export class NewProjectPage extends React.Component {

  addNewProject = pr => this.props.add(this.props.uid, pr).then(() => {
    this.props.history.push('/todo/projects');
  });

  render() {
    return (
     <div>
       <h4 className={mainHeading}>Create A New Project</h4>
       <ProjectForm type='add' submit={this.addNewProject} />
     </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    uid: state.user.uid
  }
}

NewProjectPage.propTypes = {
  uid: PropTypes.string.isRequired,
  add: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

export default connect(mapStateToProps, { add })(NewProjectPage);

import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Input } from 'semantic-ui-react'
import { dropdownWrapper } from './styles/ProjectFilterPage.css';

class ProjectFilterPage extends React.Component {

  options = [
    {key: 1, text: 'name', value: 'name'},
    {key: 2, text: 'description', value: 'comment'}
  ]

  render() {
    return (
     <div>
       <Input
         icon='search'
         iconPosition='left'
         // placeholder='Search...'
         onChange={this.props.handleSearch}
       />
       <span className={dropdownWrapper}>
          Search project by
          {' '}
          <Dropdown
            inline
            options={this.options}
            defaultValue={this.options[0].value}
            onChange={(e, {value}) => {this.props.handleTypeChange(value)}}
          />
        </span>
     </div>
    );
  }

}

ProjectFilterPage.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  handleTypeChange: PropTypes.func.isRequired
};

export default ProjectFilterPage;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Message } from 'semantic-ui-react'
import api from './../../../../api';
import ProjectForm from './../../forms/ProjectForm';
import { mainHeading } from './../pages.css';

export class UpdateProjectPage extends React.Component {

  state = {
    projectId: '',
    project: {}
  }

  componentDidMount() {
    api.project.setProjectByIdListener(this.props.uid, this.state.projectId, project => {
      this.setState({project})
    })
  }

  componentWillMount() {
    const { projectId } = this.props.match.params;
    this.setState({projectId})
  }

  componentWillUnmount() {
    api.project.removeProjectByIdListener(this.props.uid, this.state.projectId);
  }

  updateProject = update => api.project.update(this.props.uid, this.state.projectId, update)
    .then(() => {this.props.history.push('/todo/projects')} );

  render() {
    return (
     <div>
       <h4 className={mainHeading}>Update Project</h4>
       {!this.state.project &&
         <Message negative>
            <Message.Header>We're sorry we can't find project with this ID.</Message.Header>
            <p>URL could be invalid.</p>
          </Message>
        }
       <ProjectForm type='update' project={this.state.project} submit={this.updateProject} />
     </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    uid : state.user.uid
  }
}

UpdateProjectPage.propTypes = {
  uid: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(UpdateProjectPage);

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import formatDate from './../../../utils/formatDate';
import {
  List,
  Icon,
  Button,
  Loader,
  Transition,
  Popup,
  Segment
} from 'semantic-ui-react'
import {
  listItem,
  nameDateGroup,
  name,
  date,
  btnGroup,
  description,
  header,
  innerPadding
} from './styles/ProjectsListPage.css';
import ModalChoice from './../../../../common/components/elements/ModalChoice/ModalChoice';


export class ProjectsListPage extends React.Component {

  state = {
    id: '',
    modalVisible: false
  }

  projectList = projects => projects.map(pr => {

    return (
      <List.Item key={pr.id} className={listItem}>
        <Segment.Group>
          <Segment className={innerPadding} clearing>

            <div className={nameDateGroup}>
              <div className={name}>
                <List.Header className={header} as={Link} to={`/todo/tasks/${pr.id}`}>
                  {pr.name}
                </List.Header>
              </div>
              <div className={date}>{`${formatDate(pr.createdAt)}`}</div>
            </div>

            <div className={btnGroup}>
              <Popup
                trigger={
                  <Button
                    as={Link}
                    to={`${this.props.match.url}/update/${pr.id}`}
                    className='icon'
                  ><Icon name='write' /></Button>}
                content='Update the project'
              />

              <Popup
                trigger={
                  <Button
                    as={Link}
                    to={`/todo/tasks/${pr.id}`}
                    className='icon'
                  ><Icon name='tasks' /></Button>}
                content='Open the tasks of this project'
              />

              <Popup
                trigger={
                  <Button onClick={this.toggleModal.bind(null, pr.id)} className='icon'>
                    <Icon name="trash" />
                  </Button>}
                content='Delete the project'
              />
            </div>

          </Segment>

          {pr.comment &&
          <Segment className={innerPadding}>
            <p className={description}>{pr.comment}</p>
          </Segment>}

        </Segment.Group>

      </List.Item>
    )
  })

  handleRemove = id => {
    this.props.remove(this.state.id)
    this.toggleModal()
  }

  toggleModal = (id = '') => {
    this.setState({
      id, modalVisible: !this.state.modalVisible
    })
  }

  displayContent = (projects = []) => {
    if (this.props.loading) {
      return <Loader active inline />
    } else if (!projects.length) {
      return <span>No projects</span>
    } else {
      return this.projectList(projects);
    }
  }

  render() {
    return (
      <div>
        <ModalChoice
          animation='drop' // "fly left", "fade up",
          animationDuration={500}
          modalVisible={this.state.modalVisible}
          onAgree={this.handleRemove}
          onCancel={this.toggleModal}
        >
          Do you really want to delete this project?
        </ModalChoice>

        <Transition.Group
          as={List}
          duration={300}
        >
          {this.displayContent(this.props.projects)}
        </Transition.Group>
      </div>
    )
  }
}

ProjectsListPage.propTypes = {
  loading: PropTypes.bool.isRequired,
  projects: PropTypes.arrayOf(PropTypes.shape({
     name: PropTypes.string.isRequired,
     comment: PropTypes.string,
     createdAt: PropTypes.number.isRequired,
     id: PropTypes.string.isRequired
  })).isRequired,
  remove: PropTypes.func.isRequired
};

export default ProjectsListPage;

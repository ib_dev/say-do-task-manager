import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { remove, addProjectListeners, removeAllListeners } from './../../../actions/projects';
import ProjectsListPage from './ProjectsListPage';
import ProjectFilterPage from './ProjectFilterPage';
import { projectsSelector } from './../../../selectors/projectsSelector';
import { filtering } from './styles/ProjectPage.css';
import { mainHeading } from './../pages.css';


export class ProjectPage extends React.Component {

  state = {
    searchStr: '',
    searchType: 'name'
  }

  componentDidMount() {
    this.props.addProjectListeners(this.props.uid);
  }

  componentWillUnmount() {
    this.props.removeAllListeners();
  }

  remove = key => {
    this.props.remove(this.props.uid, key);
  }

  handleSearch = e => {
    this.setState({searchStr: e.target.value})
  }

  handleTypeChange = type => {
    this.setState({searchType: type})
  }

  filter = projects => projects.filter(item => item[this.state.searchType]
    .toLowerCase().includes(this.state.searchStr.toLowerCase()))


  render() {
    return (
      <div>
        <h4 className={mainHeading}>Your Projects List</h4>

        <div className={filtering}>
          <ProjectFilterPage
            handleSearch={this.handleSearch}
            handleTypeChange={this.handleTypeChange}
          />
        </div>

        <ProjectsListPage
          match={this.props.match}
          loading={this.props.loading}
          projects={this.filter(this.props.projects)}
          remove={this.remove}
        />
     </div>
    )
  }

}

const mapStateToProps = state => {
  return {
    uid: state.user.uid,
    loading: state.projects.loading,
    projects: projectsSelector(state)
  }
}

ProjectPage.propTypes = {
  uid: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  projects: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    comment: PropTypes.string,
    createdAt: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired
  })).isRequired,
  remove: PropTypes.func.isRequired,
  addProjectListeners: PropTypes.func.isRequired,
  removeAllListeners: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
  remove,
  addProjectListeners,
  removeAllListeners
})(ProjectPage)

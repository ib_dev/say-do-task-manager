import React from 'react';
import PropTypes from 'prop-types';
import { TextArea } from 'semantic-ui-react'
import { itemTextArea } from './styles/tasksGeneralStyles.css';

class TextAreaPage extends React.Component {
  state = {
    value: ''
  }

  handleUpdate = e => {
    const content = e.target.value
    clearTimeout(this.timer);
    this.setState({value: content})
    const { updateTask, task } = this.props;
    this.timer = setTimeout(updateTask.bind(null, task.id, {content}), 300)
  }

  componentWillMount() {
    this.setState({value: this.props.task.content})
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.task.content !== nextProps.task.content) {
      this.setState({value: nextProps.task.content})
    }
  }

  render() {
    return (
     <TextArea
       className={itemTextArea}
       name='content'
       onChange={this.handleUpdate}
       value={this.state.value}
       placeholder='Type here'
       rows={1}
       autoHeight
     />
    );
  }

}

TextAreaPage.propTypes = {
  updateTask: PropTypes.func.isRequired,
  task: PropTypes.shape({
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    activeAt: PropTypes.number,
    completedAt: PropTypes.number,
    activeTime: PropTypes.number,
    deadline: PropTypes.number,
    error: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired
  })
}

export default TextAreaPage;

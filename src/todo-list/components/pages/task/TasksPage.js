import React from 'react';
import PropTypes from 'prop-types';
import api from './../../../../api';
import Sticky from 'react-sticky-el';
import { connect } from 'react-redux';
import { setOpenedProject, removeOpenedProject } from './../../../actions/openedProject';
import {
  addTaskListeners,
  cleanTasksState,
  updateTask,
  reorderTasks,
  removeTask
} from './../../../actions/tasks';
import AddTaskForm from './../../forms/AddTaskForm';
import {
  activeOrderSelector,
  completedOrderSelector,
  pendingOrderSelector
} from './../../../selectors/tasksSelectors';
import { excludedTimesSelector } from './../../../selectors/excludedTimesSelector';
import TaskListPage from './TaskListPage';
import ActiveTimeSumPage from './ActiveTimeSumPage';
import PopupHeader from './../../../../common/components/elements/PopupHeader/PopupHeader';
import { projectNameStyle, allTasksWrapper, sticky, stickySlideIn, divider } from './../pages.css';


export class TasksPage extends React.Component {

  state = {
    projectId: '',
    activeTasks: null,
    completedTasks: null,
    pendingTasks: null,
    projectName: ''
  }

  componentDidMount() {
    this.props.setOpenedProject(this.state.projectId);
    this.props.addTaskListeners();
    api.project.setProjectByIdListener(this.props.uid, this.state.projectId, pr => {
      this.setState({projectName: pr.name})
    })
  }

  componentWillUnmount() {
    this.props.removeOpenedProject();
    this.props.cleanTasksState();
    api.project.removeProjectByIdListener(this.props.uid, this.state.projectId);
  }

  addTask = (content, type) => {
    return api.tasks.add({
      uid: this.props.uid,
      projectId: this.state.projectId,
      order: this.props[`${type}Order`],
      type,
      content
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { activeTasks, completedTasks, pendingTasks, match } = nextProps
    const projectId = match.params.projectId;

    if (
      (activeTasks !== prevState.activeTasks) ||
      (completedTasks !== prevState.completedTasks) ||
      (pendingTasks !== prevState.pendingTasks) ||
      (projectId !== prevState.projectId)
    ) {
      return {activeTasks, completedTasks, pendingTasks, projectId} // update state
    } else {
      return null // no changes to state
    }

  }

  handleDateChange = (id, type, selected) => {
    const selectedTs = selected.valueOf() // get Unix Timestamp (milliseconds)
    const timestamp = this.props.timestamp
    if (selectedTs < timestamp) selected.endOf('day')
    const deadline = selected.valueOf()
    this.setState({ [`${type}Tasks`]: this.updateTasks(id, type, {deadline}) })
    this.props.updateTask(id, {deadline})
  }

  updateTasks = (id, type, data) => {
    for (let key in data) var prop = key
    return this.state[`${type}Tasks`].map(task => {
      if (task.id === id) task[prop] = data[prop]
      return task;
    });
  }

  changeGroup = (origin, destination, id) => {
    const originOrder = this.props[`${origin}Order`].filter(item => item !== id);
    const destinationOrder = [id, ...this.props[`${destination}Order`]] // add to the beginning
    this.props.reorderTasks([
      { tasksType: origin, order: originOrder },
      { tasksType: destination, order: destinationOrder }
    ])
  }


  render() {
    const bulk = {
      changeGroup: this.changeGroup,
      updateTask: this.props.updateTask,
      handleDateChange: this.handleDateChange,
      timestamp: this.props.timestamp,
      excludedTimes: this.props.excludedTimes,
      remove: this.props.removeTask,
      reorder: this.props.reorderTasks
    }

    const { projectName, pendingTasks, activeTasks, completedTasks } = this.state;

    return (
     <div>
        <Sticky className={sticky} stickyClassName={stickySlideIn}>
          <AddTaskForm submit={this.addTask} />
        </Sticky>

        <div className={allTasksWrapper}>
          <PopupHeader
            popupContent='Tasks that have not yet been started.'
          >
            Pending tasks of <span className={projectNameStyle}>{projectName}</span>
          </PopupHeader>
          <div className={divider}></div>
          <TaskListPage bulk={bulk} type='pending' tasks={pendingTasks} />

          <PopupHeader
            popupContent='Tasks that are actively worked on.
            The time that a task is in this group is counted for each task separately.'
          >
            Active tasks of <span className={projectNameStyle}>{projectName}</span>
          </PopupHeader>
          <div className={divider}></div>
          <TaskListPage bulk={bulk} type='active' tasks={activeTasks} />

          <PopupHeader
            popupContent='The tasks that have been completed.
            Here is displayed the time period when a task was active.'
          >
            Completed tasks of <span className={projectNameStyle}>{projectName}</span>
          </PopupHeader>
          <div className={divider}></div>
          <TaskListPage bulk={bulk} type='completed' tasks={completedTasks} />

        </div>
        <ActiveTimeSumPage completedTasks={completedTasks} />
     </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    uid : state.user.uid,
    activeOrder: state.tasksOrders.activeOrder,
    completedOrder: state.tasksOrders.completedOrder,
    pendingOrder: state.tasksOrders.pendingOrder,
    activeTasks: activeOrderSelector(state),
    completedTasks: completedOrderSelector(state),
    pendingTasks: pendingOrderSelector(state),
    excludedTimes: excludedTimesSelector(state),
    timestamp: state.timestamp
  }
}

const tasksTypes = PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    activeAt: PropTypes.number,
    completedAt: PropTypes.number,
    activeTime: PropTypes.number,
    deadline: PropTypes.number,
    error: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired
  }))

TasksPage.propTypes = {
  uid: PropTypes.string.isRequired,
  activeOrder: PropTypes.arrayOf(PropTypes.string.isRequired),
  completedOrder: PropTypes.arrayOf(PropTypes.string.isRequired),
  pendingOrder: PropTypes.arrayOf(PropTypes.string.isRequired),
  activeTasks: tasksTypes,
  completedTasks: tasksTypes,
  pendingTasks: tasksTypes,
  excludedTimes: PropTypes.objectOf(PropTypes.array.isRequired),
  timestamp: PropTypes.number,
  setOpenedProject: PropTypes.func.isRequired,
  removeOpenedProject: PropTypes.func.isRequired,
  addTaskListeners: PropTypes.func.isRequired,
  cleanTasksState: PropTypes.func.isRequired,
  updateTask: PropTypes.func.isRequired,
  reorderTasks: PropTypes.func.isRequired,
  removeTask: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      projectId: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default connect(mapStateToProps, {
  setOpenedProject,
  removeOpenedProject,
  addTaskListeners,
  cleanTasksState,
  updateTask,
  reorderTasks,
  removeTask
})(TasksPage);

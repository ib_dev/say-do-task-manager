import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Sortable from 'react-sortablejs';
import moment from 'moment';
import DatePicker from './../../datepicker/DatePicker';
import { Form, Loader, Dropdown } from 'semantic-ui-react'
import { error } from './styles/tasksGeneralStyles.css';
import {
  itemsGroupWrapper,
  item,
  column,

  placeholder,
  dragIcon,
  itemTextAreaWrapper,
  dateAndTimeWrapper,
  dropdownWrapper,
  line,

  placeholderSelector,
  draggedItem,
  horizontalCentered
} from './styles/tasksGeneralStyles.css';
import TextAreaPage from './TextAreaPage';

const Tasks = ({bulk, type, tasks}) => {

  const {
    updateTask,
    changeGroup,
    handleDateChange,
    timestamp,
    excludedTimes,
    remove,
    reorder
  } = bulk;

  const sortableOptions = {
    group: 'tasks',
    animation: 150,
    ghostClass: `${placeholderSelector}`,  // Class name for the drop placeholder
    dragClass: `${draggedItem}`,  // Class name for the dragging item
    forceFallback: true, // Ignore the HTML5 DnD behaviour and force the fallback to kick in
    handle: ".handleSelector" // Drag handle selector within list items
  }

  const humanizeTime = task => {
    return task.activeTime ? moment.duration(task.activeTime).humanize() : 'none'
  }

  const listItems = tasks => tasks.map(task =>
    <div className={item} key={task.id} data-id={task.id}>

      {/* // Placeholder // */}
      <div className={placeholder}></div>

      {/* // Golumn // */}
      <div className={column}>

        {/* // Icon // */}
        <div className={classNames({
          handleSelector: true,
          [dragIcon]: true
        })}>
        </div>

        {/* // Textarea // */}
        <div className={itemTextAreaWrapper}>
          <TextAreaPage
            updateTask={updateTask}
            task={task}
          />
        </div>

        {/* // Datepicker // */}
        <div className={classNames({
          [dateAndTimeWrapper]: true
        })}>
          <div className={classNames({
            [horizontalCentered]: true
          })}>
            {((timestamp && type === 'active') || type === 'pending') &&
              <DatePicker
                timestamp={timestamp}
                task={task}
                type={type}
                handleDateChange={handleDateChange}
                excludedTimes={excludedTimes}
              />
            }
            {type === 'completed' && humanizeTime(task)}
          </div>
        </div>

        {/* // Dropdown // */}
        <div className={dropdownWrapper}>
          <Dropdown pointing='bottom right' icon='ellipsis vertical' floating button className='icon'>
            <Dropdown.Menu>
              {(type === 'completed' || type === 'pending') &&
                <Dropdown.Item
                  text='Move to Active Tasks'
                  onClick={() => changeGroup(type, 'active', task.id)}
                />}
              {(type === 'active' || type === 'pending') &&
                <Dropdown.Item
                  text='Move to Completed Tasks'
                  onClick={() => changeGroup(type, 'completed', task.id)}
                />}
              {(type === 'completed' || type === 'active') &&
                <Dropdown.Item
                  text='Move to Pending Tasks'
                  onClick={() => changeGroup(type, 'pending', task.id)}
                />}

              <Dropdown.Item text='Delete' onClick={() => remove(task.id, type)} />
            </Dropdown.Menu>
          </Dropdown>
        </div>

      </div>

      {/* // Line under task // */}
      <div className={classNames({
        [line]: true,
        [error]: task.error
      })}></div>

    </div>
  );

  const displayContent = tasks => {
    if (!tasks) {
      return <Loader active inline />
    } else if (!tasks.length) {
      return `No ${type} tasks`
    } else {
      return listItems(tasks);
    }
  }

  return (
    <Form>
      <Sortable
       className={itemsGroupWrapper}
       options={sortableOptions}
       onChange={order => {
        reorder([{
          tasksType: type,
          order
        }]);
       }}
      >
        {displayContent(tasks)}
      </Sortable>
    </Form>
  );

}

Tasks.propTypes = {
  bulk: PropTypes.shape({
    changeGroup: PropTypes.func.isRequired,
    excludedTimes: PropTypes.objectOf(PropTypes.array.isRequired),
    handleDateChange: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired,
    reorder: PropTypes.func.isRequired,
    updateTask: PropTypes.func.isRequired,
    timestamp: PropTypes.number
  }),
  type: PropTypes.string.isRequired,
  tasks: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    activeAt: PropTypes.number,
    completedAt: PropTypes.number,
    activeTime: PropTypes.number,
    deadline: PropTypes.number,
    error: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired
  }))
};

export default Tasks;

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Tab } from 'semantic-ui-react'
import round from './../../../../common/utils/round.js';

class ActiveTimeSum extends React.Component {

  state = {
    activeIndex: 0,
    show: false
  }

  sumActiveTime = type => {
    const tasks = this.props.completedTasks ? this.props.completedTasks : []
    const time = tasks.reduce( (accumulator, currentValue) => {
      return accumulator + (currentValue.activeTime ? currentValue.activeTime : 0);
    }, 0)

    if (!time || !type) return time;

    const duration = moment.duration(time);
    return type !== 'Humanized' ? round(duration['as'+ type](), 1)
      + ` ${type.toLowerCase()}.` : (duration.humanize() + '.')
  }

  componentWillMount() {
    this.setState({show: !!this.sumActiveTime()})
  }

  componentWillReceiveProps(nextProps) {
    this.setState({show: !!this.sumActiveTime()})
  }

  paneIntroText = 'The completed tasks have been active all together about '

  panes = [
    { menuItem: 'Humanized', render: () => {
        return <Tab.Pane attached='top'>
          {this.paneIntroText}<strong>{this.sumActiveTime('Humanized')}</strong>
        </Tab.Pane>
      }
    },
    { menuItem: 'Hours', render: () => {
        return <Tab.Pane attached='top'>
          {this.paneIntroText}<strong>{this.sumActiveTime('Hours')}</strong>
        </Tab.Pane>
      }
    },
    { menuItem: 'Days', render: () => {
        return <Tab.Pane attached='top'>
          {this.paneIntroText}<strong>{this.sumActiveTime('Days')}</strong>
        </Tab.Pane>
      }
    },
    { menuItem: 'Minutes', render: () => {
        return <Tab.Pane attached='top'>
          {this.paneIntroText}<strong>{this.sumActiveTime('Minutes')}</strong>
        </Tab.Pane>
      }
    }
  ]

  handleTabChange = (e, { activeIndex }) => this.setState({ activeIndex })

  render() {
    return (
      <div>
        {this.state.show &&
          <Tab
            menu={{ attached: 'bottom' }}
            panes={this.panes}
            activeIndex={this.state.activeIndex}
            onTabChange={this.handleTabChange}
          />
        }
      </div>
    )
  }
}

ActiveTimeSum.propTypes = {
  activeTasks: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    activeAt: PropTypes.number,
    completedAt: PropTypes.number,
    activeTime: PropTypes.number,
    deadline: PropTypes.number,
    error: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired
  }))
}

export default ActiveTimeSum;

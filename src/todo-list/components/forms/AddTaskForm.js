import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message, TextArea, Dropdown, Segment } from "semantic-ui-react";
import TextError from './../../../common/components/messages/TextError/TextError';
import { wrapper, textAreaWrapper, buttonWrapper } from './style/AddTaskForm.css';


class AddTaskForm extends React.Component {
  state = {
    data: {
      content: ''
    },
    type: 'pending',
    loading: false,
    errors: {}
  }

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data, this.state.type)
        .then(() => {
          this.setState({
            loading: false,
            data: {
              content: ''
            }
          })
        })
        .catch(err =>
          this.setState({
            ...this.state,
            errors: { global: err.message },
            loading: false
          })
        );
    }
  };

  validate = data => {
    const errors = {};
    const contentLength = data.content.trim().length
    if (contentLength < 3 || contentLength > 2000)
      errors.content = "Type between 3 and 2000 characters and submit again!";
    return errors;
  };

  dropdownOptions = [
    {key: 1, text: 'pending tasks', value: 'pending'},
    {key: 2, text: 'active tasks', value: 'active'},
    {key: 3, text: 'completed tasks', value: 'completed'}
  ]

  handleTypeChange = (e, {value}) => {
    this.setState({
      ...this.state,
      type: value
    })
  }

  render() {
    const {data, errors, loading} = this.state;

    return (
      <Segment raised>
        <Form onSubmit={this.onSubmit} loading={loading} noValidate>
          <span>
             Add to
             {' '}
             <Dropdown
               inline
               options={this.dropdownOptions}
               defaultValue={this.dropdownOptions[0].value}
               onChange={this.handleTypeChange}
             />
           </span>

          {!!errors.global &&
            <Message negative>
              Something went wrong!
              <p>{errors.global}</p>
            </Message>
          }
          <div className={wrapper}>
            <div className={textAreaWrapper}>
              <Form.Field error={!!errors.content}>
                <TextArea
                  name='content'
                  onChange={this.onChange}
                  value={data.content}
                  placeholder='Write task here'
                  rows={1}
                  autoHeight
                />
              </Form.Field>
            </div>
            <div className={buttonWrapper}><Button primary>ADD</Button></div>
          </div>
          { errors.content && <TextError text={errors.content} /> }
        </Form>
      </Segment>
    );
  }

}

AddTaskForm.propTypes = {
  submit: PropTypes.func.isRequired
};

export default AddTaskForm;

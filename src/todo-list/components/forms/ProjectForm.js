import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message, TextArea } from "semantic-ui-react";
import TextError from './../../../common/components/messages/TextError/TextError';
import { input, textArea } from './style/ProjectForm.css';

class AddProjectForm extends React.Component {
  state = {
    data: {
      name: '',
      comment: ''
    },
    loading: true,
    errors: {}
  }

  componentWillMount() {
    if (this.props.type === 'add') this.setState({ loading: false });
  }

  componentWillReceiveProps(nextProps) {
    if ((nextProps.project !== this.props.project) && (this.props.type === 'update')) {
      this.setState({
        ...this.state,
        loading: false,
        data: { ...this.state.data, ...nextProps.project }
      });
    }
  }

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err =>
          this.setState({
            ...this.state,
            errors: { global: err.message },
            loading: false
          })
        );
    }
  };

  validate = data => {
    const errors = {};
    if ((data.name.trim().length <= 2) || (data.name.trim().length > 49))
      errors.name = "Type between 2 and 50 characters and submit again!";
    if (data.comment.trim().length > 2000)
      errors.comment = "Type below 2000 characters and submit again!";
    return errors;
  };

  render() {
    const {data, errors, loading} = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading} noValidate>
        {!!errors.global &&
          <Message negative>
            Something went wrong!
            <p>{errors.global}</p>
          </Message>
        }
        <Form.Field error={!!errors.name}>
          <label htmlFor="name">Name</label>
            <input
              className={input}
              type="text"
              id="name"
              name="name"
              placeholder="Write your project name here"
              value={data.name}
              onChange={this.onChange} />
          {
            errors.name && <TextError text={errors.name} />
          }
        </Form.Field>

        <Form.Field error={!!errors.comment}>
          <label htmlFor="comment">Description</label>
            <TextArea
              className={textArea}
              name='comment'
              onChange={this.onChange}
              value={data.comment}
              placeholder='Write your project description here'
              rows={1}
              autoHeight
            />
          {
            errors.comment && <TextError text={errors.comment} />
          }
        </Form.Field>
        <Button primary>{this.props.type === 'add' ? 'SUBMIT' : 'UPDATE'}</Button>
      </Form>
    );
  }

}

AddProjectForm.propTypes = {
  submit: PropTypes.func.isRequired,
  project: PropTypes.shape({
     comment: PropTypes.string,
     name: PropTypes.string,
     createdAt: PropTypes.number
  }),
  type: PropTypes.string.isRequired
};

export default AddProjectForm;

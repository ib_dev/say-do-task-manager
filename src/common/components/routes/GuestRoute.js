
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

export const GuestRoute = ({emailVerified, component: Component, ...rest}) => {
  return (
    <Route {...rest} render={props => {
      return !emailVerified ? <Component {...props} /> : <Redirect to="/todo/projects" />
    }} />
  )
};

GuestRoute.propTypes = {
  component: PropTypes.func.isRequired,
  emailVerified: PropTypes.bool.isRequired
}

export function mapStateToProps(state){
  return {
    emailVerified: !!state.user.emailVerified
  }
}

export default connect(mapStateToProps)(GuestRoute);

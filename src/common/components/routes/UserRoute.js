import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Redirect, withRouter } from 'react-router-dom';

export const UserRoute = ({emailVerified, component: Component, ...rest}) => {
  return (
    <Route {...rest} render={props => {
      return emailVerified ? <Component {...props} /> : <Redirect to="/" />
    }} />
  )
};

UserRoute.propTypes = {
  component: PropTypes.func.isRequired,
  emailVerified: PropTypes.bool.isRequired
}

export function mapStateToProps(state){
  return {
    emailVerified: !!state.user.emailVerified
  }
}

export default withRouter(connect(mapStateToProps)(UserRoute));

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const RedirectPage = ({ emailVerified }) => (
  <div>
    {emailVerified ? (
      <Redirect to="/todo/projects" />
    ) : (
      <Redirect to="/auth/login" />
    )}
  </div>
);

RedirectPage.propTypes = {
  emailVerified: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    emailVerified: !!state.user.emailVerified
  };
}

export default connect(mapStateToProps)(RedirectPage); 

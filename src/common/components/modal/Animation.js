import React, { Component } from 'react';
import { Transition } from 'semantic-ui-react';

class Animation extends Component {

  state = {
    show: false
  }

  componentDidMount() {
    this.setState({show: this.props.showOnMount })
  }

  componentWillReceiveProps(nextProps) {
    this.setState({show: nextProps.show })
  }

  render() {
    const show = this.state.show !== undefined ? this.state.show : false;
    const { bg, centered } = this.props.styles;

    if (!this.props.animation && show) {
      return (<div className={bg}>{this.props.children}</div>);
    } else {
      return (
        <Transition visible={show} animation={this.props.animation} duration={this.props.duration}>
          <div className={bg}>
            <div className={centered}>{this.props.children}</div>
          </div>
        </Transition>
      )
    }
  }

}

export default Animation;

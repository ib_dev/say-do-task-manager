import React from 'react';
import PropTypes from 'prop-types';

// Use inside modal
const MLink = (props) => {

  const handleClick = (e) => {
    e.preventDefault();
    props.history.push(props.to)
  }

  return (
    <a href={props.to} onClick={(e) => handleClick(e)}>
      {props.children}
    </a>
  );
}

MLink.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }),
  to: PropTypes.string.isRequired
};

export default MLink;

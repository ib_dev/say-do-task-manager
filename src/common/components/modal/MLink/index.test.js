import React from 'react';
import { shallow } from 'enzyme';
import MLink from './';


const historyMock = {
  push: jest.fn()
};
const component = shallow(<MLink to="/path" history={historyMock}>Click me</MLink>);

beforeAll(() => {
  component.find('a').simulate('click', { preventDefault: jest.fn() });
});

it('should call historyMock.push with "/path" as argument', () => {
  //console.log(component.debug());
  expect(historyMock.push).toBeCalledWith("/path");
});

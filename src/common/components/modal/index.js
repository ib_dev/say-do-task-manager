/* How to use?

import Modal, { MLink } from '...';

<Modal show={true} animation="fly left" duration={1500}>
  // history and to props are isRequired
  <MLink to="/dashboard" history={history}>Dashboard</MLink>
  <MLink to="/books/new" history={history}>Add New Book</MLink>
</Modal>

*/

import React, { Component } from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { Provider } from 'react-redux'; // for using redux inside modal
import store from './../../../store';
import Animation from './Animation.js';
import styles from './modalStyles.css';

class Modal extends Component {

  state = {
    show: false,
    showOnMount: false
  }

  componentDidMount() {
    this.modalTarget = document.createElement('div');
    this.modalTarget.className = 'modal';
    document.body.appendChild(this.modalTarget);

    if (this.props.show !== undefined) {
      this.setState({...this.state, showOnMount: this.props.show});
    }
    this._render();
  }

  componentWillUpdate(nextProps) {
    if (this.state.show !== nextProps.show) {
      this._render(nextProps.show);
      this.setState({...this.state, show: nextProps.show});
    }
  }

  componentWillUnmount() {
    unmountComponentAtNode(this.modalTarget);
    document.body.removeChild(this.modalTarget);
  }

  _render(show = false) {

    render(
      <Provider store={store}>
        <Animation
          styles={styles}
          showOnMount={this.state.showOnMount}
          {...this.props}
          show={show}/>
      </Provider>,
      this.modalTarget
    );
  }

  render() {
    return (
      <noscript />
    );
  }

}

export { default as MLink } from './MLink/';
export default Modal;

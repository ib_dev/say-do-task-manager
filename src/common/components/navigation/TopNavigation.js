import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import classNames from 'classnames';
import { Menu, Dropdown, Icon } from 'semantic-ui-react';
import * as actions from './../../../auth/actions/auth';
import { rightDropdown, menu } from './topNavigation.css';


class TopNavigation extends React.Component {
  render() {
    const { logout } = this.props;

    return (
      <div className={menu}>
        <Menu pointing>
          <Menu.Item
            as={Link} to="/todo/projects"
          >
            <Icon name='home' color='blue' />
          </Menu.Item>
          {this.props.projectListIsActive && <Menu.Item
            as={Link} to="/todo/projects/add-new"
          >
            Create A New Project
          </Menu.Item>}
          <Menu.Menu position="right">
            <Dropdown icon='log out' button className={classNames({
              'icon': true,
              [rightDropdown]: true
            })}>
              <Dropdown.Menu>
                <Dropdown.Item onClick={()=> logout()}>Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>
      </div>
    )
  }

}

TopNavigation.propTypes = {
  projectListIsActive: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    projectListIsActive: !!Object.keys(state.listeners.projectAdded).length
  }
}

export default connect(mapStateToProps, {logout: actions.logout})(TopNavigation);

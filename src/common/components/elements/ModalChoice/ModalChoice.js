import React from 'react';
import PropTypes from 'prop-types';
import { Message, Button, Segment } from 'semantic-ui-react';
import Modal from './../../modal/';
import { content, message, buttonGroup, btnMargin, text } from './ModalChoice.css';

const ModalChoice = ({modalVisible, onAgree, onCancel, children, animation, animationDuration }) => {

  return (
    <Modal show={modalVisible} animation={animation} duration={animationDuration}>
      <Segment raised>
        <div className={content}>
          <Message className={message} info>
            <span className={text}>{children}</span>
          </Message>
          <div className={buttonGroup}>
            <Button onClick={() => {onAgree()}} primary>OK</Button>
            <Button onClick={() => {onCancel()}} className={btnMargin}>CANCEL</Button>
          </div>
        </div>
      </Segment>
    </Modal>
  )
}

ModalChoice.propTypes = {
  animationDuration: PropTypes.number,
  animation: PropTypes.string,
  onAgree: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
}

export default ModalChoice;

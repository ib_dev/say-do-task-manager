import React from 'react';
import './Gaegu.css';
import './IndieFlower.css';

import { logo, explanation } from './Logo.css';

const Logo = () => {

  return (
    <div className={logo}>
      Say&Do <div className={explanation}>Task Manager</div>
    </div>
    );
}

export default Logo;

import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Popup } from 'semantic-ui-react'
import {
  popupHeader,
  groupHeading,
  groupHeadingIcon,
  iconWrapper
} from './popupHeaderStyles.css';

const PopupHeader = ({children, popupContent}) => {
  return (
    <div className={popupHeader}>
      <h5 className={groupHeading}>{children}</h5>
      <Popup
        on='click'
        trigger={
          <div className={iconWrapper}>
            <Icon className={groupHeadingIcon} name='info circle' />
          </div>
        }
        content={popupContent}
      />
    </div>
  )
}

PopupHeader.propTypes = {
  children: PropTypes.node.isRequired,
  popupContent: PropTypes.string.isRequired
}

export default PopupHeader;

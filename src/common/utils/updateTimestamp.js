import api from './../../api';
import setTimer from 'set-timer';
import { timestampCreated } from './../actions/timestamp';

const updateTimestamp = store => {
  api.general.getTimestamp().then(timestamp => {
    store.dispatch(timestampCreated(timestamp))

    let interval = 500
    setTimer(() => {
      timestamp = timestamp + interval
      store.dispatch(timestampCreated(timestamp))
    }, {
      timeout: interval,       // Wait 0.5 second before first call.
      limit: 7200,            // Call callback 3600 times (new api call every hour).
      interval: interval,     // Wait 0.5 second between calls.
      onClear: () => {        // Call after timer is cleared.
        updateTimestamp(store)
      }
    });
  })
}

export default updateTimestamp;

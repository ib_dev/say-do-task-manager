import { TIMESTAMP_CREATED } from './../../types';

export const timestampCreated = timestamp => ({
  type: TIMESTAMP_CREATED, timestamp
});

import { TIMESTAMP_CREATED } from "./../../types";

export default function(state = null, action = {}) {
  switch (action.type){
    case TIMESTAMP_CREATED:
      return action.timestamp
    default:
      return state;
  }
}

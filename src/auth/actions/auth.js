import { USER_LOGGED_IN, USER_LOGGED_OUT } from "../../types";
import api from '../../api';

export const userLoggedIn = user => ({
  type: USER_LOGGED_IN,
  user
});

export const userLoggedOut = () => ({
  type: USER_LOGGED_OUT
});

export const login = credentials => dispatch => {
  return api.user.login(credentials).then(user => {
    sessionStorage.setItem('user', JSON.stringify(user));
    if (user.emailVerified) dispatch(userLoggedIn(user));
    return user; 
  });
}

export const logout = () => dispatch => {
  return api.user.signOut().then(() => {
    sessionStorage.removeItem('user');
    dispatch(userLoggedOut());
  });
};

import api from '../../api';
import { userLoggedIn } from './auth';

export const signup = (data) => (dispatch) =>
  api.user.signup(data).then(user => {
    sessionStorage.setItem('user', JSON.stringify(user));
    if (user.emailVerified) dispatch(userLoggedIn(user));
    return user;
})

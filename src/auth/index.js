import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import GuestRoute from './../common/components/routes/GuestRoute';

import LoginPage from "./components/pages/LoginPage/LoginPage";
import SignupPage from "./components/pages/SignupPage";
import ForgotPasswordPage from './components/pages/ForgotPasswordPage';
import VerificationPage from './components/pages/VerificationPage';
import NotFoundPage from './../common/components/pages/NotFoundPage';
import { wrapper } from './index.css';

const Auth = ({location, match}) => {
  return (
    <div className={wrapper}>
      <Switch>
        <GuestRoute path={match.url + "/login"} exact component={LoginPage} location={location}/>
        <GuestRoute path={match.url + "/signup"} exact component={SignupPage} location={location}/>
        <GuestRoute path={match.url + "/forgot_password"} exact component={ForgotPasswordPage} location={location}/>
        <GuestRoute path={match.url + "/verification/:status"} exact component={VerificationPage} location={location}/>
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
}

Auth.propTypes = {
  location: PropTypes.shape({
    pathname:  PropTypes.string.isRequired
  }).isRequired
}

export default Auth;

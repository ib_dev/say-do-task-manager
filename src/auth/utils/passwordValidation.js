import passwordValidator from 'password-validator';

export const passwordErrors = {
  min: 'Minimum length 8.',
  max: 'Maximum length 50.',
  uppercase: 'Must have uppercase letters.',
  lowercase: 'Must have lowercase letters.',
  digits: 'Must have digits.',
  spaces: 'Should not have spaces.'
}

export const isValidPassword = pass => {
  const schema = new passwordValidator();
  schema
    .is().min(8)                                    // Minimum length 8
    .is().max(50)                                  // Maximum length 50
    .has().uppercase()                              // Must have uppercase letters
    .has().lowercase()                              // Must have lowercase letters
    .has().digits()                                 // Must have digits
    .has().not().spaces();                          // Should not have spaces

  return schema.validate(pass, { list: true })
}

export const errorMessage = shortcomings => {
  return shortcomings.reduce( (accumulator, currentValue) => {
    return accumulator + passwordErrors[currentValue] + '\n'
  }, '')
}

import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message, Segment } from "semantic-ui-react";
import { isEmail } from "validator";
import TextError from "./../../../common/components/messages/TextError/TextError";
import { isValidPassword, errorMessage } from './../../utils/passwordValidation';
import { label } from './forms.css';

class SignupForm extends React.Component {
  state = {
    data: {
      email: '',
      password: '',
      passwordConfirmation: ''
    },
    loading: false,
    errors: {}
  }

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .then(() => {
          this.setState({
            loading: false,
            data: {
              email: "",
              password: ""
            }
          })
        })
        .catch(err =>
          this.setState({
            ...this.state,
            errors: { global: err.message },
            loading: false
          })
        );
    }
  };

  validate = data => {
    const errors = {};
    if (!isEmail(data.email)) errors.email = "Invalid email";
    if (data.password !== data.passwordConfirmation)
      errors.passwordConfirmation = "Passwords must match."
    const shortcomings = isValidPassword(data.password)
    if (shortcomings.length) errors.password = errorMessage(shortcomings)
    return errors;
  };

  render() {
    const {data, errors, loading} = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading} noValidate>
        <Segment padded>
          {errors.global && (
            <Message negative>
              <Message.Header>Something went wrong</Message.Header>
              <p>{errors.global}</p>
            </Message>
          )}

          <Form.Field error={!!errors.email}>
            <label className={label} htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="example@example.com"
              value={data.email}
              onChange={this.onChange}
            />
            {errors.email && <TextError text={errors.email} />}
          </Form.Field>

          <Form.Field error={!!errors.password}>
            <label className={label} htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Your password"
              value={data.password}
              onChange={this.onChange}
            />
            {errors.password && <TextError text={errors.password} />}
          </Form.Field>

          <Form.Field error={!!errors.passwordConfirmation}>
            <label className={label} htmlFor="passwordConfirmation">
              Confirm your password
            </label>
            <input
              type="password"
              id="passwordConfirmation"
              name="passwordConfirmation"
              placeholder="Type password again"
              value={data.passwordConfirmation}
              onChange={this.onChange}
            />
            {errors.passwordConfirmation && (
              <TextError text={errors.passwordConfirmation} />
            )}
          </Form.Field>

          <Button fluid primary>Create Account</Button>
      </Segment>
      </Form>
    );
  }

}

SignupForm.propTypes = {
  submit: PropTypes.func.isRequired
}

export default SignupForm;

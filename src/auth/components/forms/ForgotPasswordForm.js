import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message, Segment } from "semantic-ui-react";
import { isEmail } from "validator";
import TextError from "./../../../common/components/messages/TextError/TextError";

class ForgotPasswordForm extends React.Component {
  state = {
    data: {
      email: ''
    },
    loading: false,
    errors: {}
  }

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .then(() => {
          this.setState({
            loading: false,
            data: {
              email: "",
              password: ""
            }
          })
        })
        .catch(err =>
          this.setState({
            ...this.state,
            errors: { global: err.message },
            loading: false
          })
        );
    }
  };

  validate = data => {
    const errors = {};
    if (!isEmail(data.email)) errors.email = "Invalid email";
    return errors;
  };

  render() {
    const {data, errors, loading} = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading} noValidate>
        <Segment padded>

          {!!errors.global && <Message negative>{errors.global}</Message>}

          <Form.Field error={!!errors.email}>
            <p>
              Enter your email address that you used to register.<br/>
              We'll send you an email with a link so that you can reset your password.
            </p>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="example@example.com"
              value={data.email}
              onChange={this.onChange}
            />
            {errors.email && <TextError text={errors.email} />}
          </Form.Field>

          <Button fluid primary>Send</Button>

        </Segment>
      </Form>
    );
  }

}

ForgotPasswordForm.propTypes = {
  submit: PropTypes.func.isRequired
}

export default ForgotPasswordForm;

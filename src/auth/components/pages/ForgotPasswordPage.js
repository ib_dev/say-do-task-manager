import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Message } from 'semantic-ui-react';
import classNames from 'classnames';
import api from './../../../api';
import ForgotPasswordForm from './../forms/ForgotPasswordForm';
import { grid, backLink, backLinkPosition } from './pages.css';

class ForgotPasswordPage extends Component {
  state = {
    success: false
  }

  submit = ({email}) => api.user.passwordResetEmail(email).then(() => {
    this.setState({success: true})
  })

  render() {
    const link = classNames({
      [backLink]: true,
      [backLinkPosition]: this.state.success
    })

    return (
      <Grid className={grid} verticalAlign='middle' centered>
        <Grid.Column mobile={16} tablet={8} computer={8}>
          {this.state.success ?
            <Message positive>
              Email has been sent.
            </Message> :
            <ForgotPasswordForm submit={this.submit} />
          }
          <Link to="/auth/login" className={link}>Go Back</Link>
        </Grid.Column>
      </Grid>
    );
  }

}

export default ForgotPasswordPage;

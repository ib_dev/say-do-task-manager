import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Grid, Button, Divider, Segment } from 'semantic-ui-react';
import LoginForm from "../../../components/forms/LoginForm";
import { login } from "../../../actions/auth";
import Logo from './../../../../common/components/elements/Logo/Logo.js';
import { wrapper, grid, forgotPasswordLink, divider } from './LoginPage.css';


class LoginPage extends React.Component {
  submit = credentials =>  {
    return this.props.login(credentials).then(user => {
      if (user.emailVerified) {
        this.props.history.push("/todo/projects");
      } else {
        this.props.history.push("/auth/verification/success");
      }
    });
  }

  render() {
    return (
      <div className={wrapper}>
        <Grid className={grid} verticalAlign='middle' centered>
          <Grid.Column mobile={16} tablet={6} computer={6}>
            <Logo />
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>

            <Segment padded>
              <LoginForm submit={this.submit} />
              <Link className={forgotPasswordLink} to="/auth/forgot_password">Forgot Password?</Link>
              <Divider className={divider} horizontal>Or</Divider>
              <Button fluid secondary as={Link} to="/auth/signup">Sign Up</Button>
            </Segment>

          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

export default connect(null, { login })(LoginPage);

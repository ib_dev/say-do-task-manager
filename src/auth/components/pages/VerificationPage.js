import React from 'react';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';
import { Grid, Message, Button, Segment } from 'semantic-ui-react';
import api from './../../../api';
import { grid, backLink, segment } from './pages.css';


class VerificationPage extends React.Component {

  state = {
    error: false
  }

  componentDidMount() {
    const status = this.props.match.params.status;
    if (status === 'error') this.setState({error: true})
  }

  sendVerifigation = () => {
    api.user.verification().then(() => {
      this.setState({error: false})
    })
    .catch(errMsg => {
      this.setState({error: true})
    })
  }

  render() {
    return (
      <Grid className={grid} verticalAlign='middle' centered>
        <Grid.Column mobile={16} tablet={9} computer={9}>

          {this.state.error && <Message negative attached='top'>
            <Message.Header>
              An error occurred, please send the verification e-mail again.
            </Message.Header>
          </Message>}

          {!this.state.error && <Message positive attached='top'>
            <Message.Header>
              A verification link has been sent to your email address which you used to register!
            </Message.Header>
          </Message>}

          <Segment className={segment} attached>
            <p>Click the button below if you don't find the verification e-mail.</p>
            <Button fluid compact onClick={this.sendVerifigation}>Send the verification e-mail again</Button>
          </Segment>
          <Link className={backLink} to="/auth/login">Go To Login</Link>

        </Grid.Column>
      </Grid>
    )
  }
}

VerificationPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      status: PropTypes.string
    })
  })
}

export default VerificationPage;

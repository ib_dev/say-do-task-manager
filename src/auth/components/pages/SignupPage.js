import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import SignupForm from '../forms/SignupForm';
import { signup } from '../../actions/users';
import api from './../../../api';
import { grid, backLink } from './pages.css';

class SignupPage extends React.Component {
  submit = data =>
    this.props.signup(data).then(() => {
      api.user.verification().then(() => {
        this.props.history.push('/auth/verification/success')
      })
      .catch(err => {
        this.props.history.push('/auth/verification/error')
      });
    });

  render() {
    return (
      <Grid className={grid} verticalAlign='middle' centered>
        <Grid.Column mobile={16} tablet={8} computer={8}>
          <SignupForm submit={this.submit} />
          <Link className={backLink} to="/auth/login">Go Back</Link>
        </Grid.Column>
      </Grid>
    );
  }

}

const mapStateToProps = state => {
  return {
    emailVerified: !!state.user.emailVerified
  }
}

SignupPage.propTypes = {
  emailVerified: PropTypes.bool.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  signup: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { signup })(SignupPage);

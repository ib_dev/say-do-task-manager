import { TIMESTAMP_CREATED } from './types';
import { createStore, applyMiddleware } from "redux";
import { createLogger } from 'redux-logger'
import thunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import rootReducer from './rootReducer';
import listenTasks from './todo-list/middlewares/listenTasks';
import { activeOrder } from './todo-list/middlewares/tasksOrder';
import updateTask from './todo-list/middlewares/updateTask';
import removeTask from './todo-list/middlewares/removeTask';


const middlewares = [
  thunk,
  listenTasks,
  activeOrder,
  updateTask,
  removeTask
];
if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    predicate: (getState, action) => action.type !== TIMESTAMP_CREATED
  });
  middlewares.push(logger);
}

const middleware = applyMiddleware(...middlewares);
const composeEnhancers = composeWithDevTools({/* Options */});

export default createStore(
  rootReducer,
  {},
  composeEnhancers(middleware)
);

// auth
export const USER_LOGGED_IN = "USER_LOGGED_IN";
export const USER_LOGGED_OUT = "USER_LOGGED_OUT";

// task manager
export const PROJECT_ADDED = "PROJECT_ADDED";
export const PROJECT_REMOVED = "PROJECT_REMOVED";
export const PROJECT_UPDATED = "PROJECT_UPDATED";
export const PROJECT_OPENED = "PROJECT_OPENED";
export const PROJECT_CLOSED = "PROJECT_CLOSED";
export const NEW_ACTIVE_ORDER = "NEW_ACTIVE_ORDER";
export const NEW_ACTIVE_ORDER_LISTENER = "NEW_ACTIVE_ORDER_LISTENER";
export const TASK_ADDED = "TASK_ADDED";
export const TASK_REMOVED = "TASK_REMOVED";
export const TASKS_LISTENER = "TASKS_LISTENER";
export const REMOVE_ALL_LISTENERS = "REMOVE_ALL_LISTENERS";
export const REMOVE_ALL_TASKS = "REMOVE_ALL_TASKS";
export const REMOVE_TASKS_ORDERS = "REMOVE_TASKS_ORDERS";
export const LISTEN_TASKS_ORDER = "LISTEN_TASKS_ORDER";
export const LISTEN_TASKS = "LISTEN_TASKS";
export const UPDATE_TASK = "UPDATE_TASK";
export const TASK_UPDATED = "TASK_UPDATED";
export const REORDER_TASKS = "REORDER_TASKS";
export const NEW_COMPLETED_ORDER = "NEW_COMPLETED_ORDER";
export const NEW_COMPLETED_ORDER_LISTENER = "NEW_COMPLETED_ORDER_LISTENER";
export const REMOVE_TASK = "REMOVE_TASK";
export const TIMESTAMP_CREATED = "TIMESTAMP_CREATED";
export const PROJECTS_LISTENER = "PROJECTS_LISTENER";
export const PROJECTS_REQUESTED = "PROJECTS_REQUESTED";
export const NEW_PENDING_ORDER_LISTENER = "NEW_PENDING_ORDER_LISTENER";
export const NEW_PENDING_ORDER = "NEW_PENDING_ORDER";

import user from "./auth/reducers/user";
import projects from './todo-list/reducers/projects';
import tasks from './todo-list/reducers/tasks';
import tasksOrders from './todo-list/reducers/tasksOrders';
import openedProject from './todo-list/reducers/openedProject';
import listeners from './todo-list/reducers/listeners';
import timestamp from './common/reducers/timestamp';
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  user,
  projects,
  tasks,
  tasksOrders,
  openedProject,
  listeners,
  timestamp
});

export default rootReducer;

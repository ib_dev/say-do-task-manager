import * as fb from "firebase";

const CONFIG = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID
};

// Initialize Firebase
fb.initializeApp(CONFIG);

const timestampUrl = 'https://us-central1-fir-todo-list-da99d.cloudfunctions.net/timestamp'
const db = fb.database();
const timestamp = fb.database.ServerValue.TIMESTAMP
const domain = process.env.NODE_ENV === 'production' ?
'https://' + process.env.REACT_APP_FIREBASE_DOMAIN : 'http://localhost:3000';

export { timestampUrl, db, timestamp, domain };
export default fb;

import React from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

// common
import RedirectPage from "./common/components/pages/RedirectPage";
import TopNavigation from './common/components/navigation/TopNavigation';
import NotFoundPage from './common/components/pages/NotFoundPage';

import Auth from './auth/';
import TodoList from './todo-list/';
import { content } from './scss/content.css';

export const App = ({location, emailVerified}) => {
  return (
    <div className='ui container'>
      <div className={content}>
        {emailVerified && <TopNavigation />}
        <Switch>
          <Route path="/" exact component={RedirectPage} location={location} />
          <Route path="/auth" component={Auth} location={location} />
          <Route path="/todo" component={TodoList} location={location} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    </div>
  )
}

export function mapStateToProps(state) {
  return {
    emailVerified: !!state.user.emailVerified
  }
}

App.propTypes = {
  emailVerified: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    pathname:  PropTypes.string.isRequired
  }).isRequired
}

export default connect(mapStateToProps)(App);

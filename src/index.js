// polyfill
import "babel-polyfill";

import 'semantic-ui-css/semantic.min.css';
import { render } from "react-dom";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from 'react-router-dom';

import App from "./App";
import store from "./store";
import { userLoggedIn } from "./auth/actions/auth";
import credentials from './auth/utils/extractUserCredentials';
import updateTimestamp from './common/utils/updateTimestamp';

updateTimestamp(store);

const userData = sessionStorage.getItem('user');
if (userData) {
  const user = JSON.parse(userData);
  store.dispatch(userLoggedIn(credentials(user)));
}

render(
    <BrowserRouter>
      <Provider store={store}>
        <Route component={App} />
      </Provider>
    </BrowserRouter>,
  window.document.getElementById('app')
);

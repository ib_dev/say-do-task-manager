import axios from 'axios';
import fb, { db, timestamp, timestampUrl, domain } from './fb';
import credentials from './auth/utils/extractUserCredentials';

export const handleChildAdded = (ref, lastCreatedAt, action) => {
  const future = lastCreatedAt + 1;
  const finalRef = lastCreatedAt ? ref.orderByChild('createdAt').startAt(future) : ref

  finalRef.on('child_added', snap => {
    const item = {[snap.key]: snap.val()};
    action(item);
  })
}

export const handleFetchAll = listenerRef => {
  return listenerRef.once('value').then(snap => {
    const data = snap.val()
    let lastCreatedAt = 0;
    if (data) {
      lastCreatedAt = Object.keys(data).reduce((m, k) => {
        return data[k].createdAt > m ? data[k].createdAt : m
      }, -Infinity );
    }
    return {data, lastCreatedAt, listenerRef}
  })
}

export const getProjectRef = (uid, key) => db.ref(`todo-list/${uid}/projects/${key}`);
export const getAllProjectsRef = uid => db.ref(`todo-list/${uid}/projects`);


export default {
  general: {
    getTimestamp: () => axios.get(timestampUrl).then(res => res.data)
  },
  user: {
    signup: ({email, password}) => fb.auth().createUserWithEmailAndPassword(email, password)
      .then(user => credentials(user)),
    login: ({email, password}) => fb.auth().signInWithEmailAndPassword(email, password).then(user => {
      fb.auth().currentUser.getIdToken(true)
      return credentials(user);
    }),
    signOut: () => fb.auth().signOut(),
    verification: () => {
      const user = fb.auth().currentUser;
      return user.sendEmailVerification({url: domain + '/auth/login'}) // redirection url
    },
    passwordResetEmail: email => fb.auth().sendPasswordResetEmail(
      email,
      {url: domain + '/auth/login'} // redirection url
    )
  },
  project: {
    setProjectByIdListener: (uid, key, action) => {
      getProjectRef(uid, key).on('value', snap => action(snap.val()));
    },
    removeProjectByIdListener: (uid, key) => {
      getProjectRef(uid, key).off();
    },
    add: (uid, {name, comment}) => {
      const projectRef = getAllProjectsRef(uid).push();
      return projectRef.set({name, comment, createdAt: timestamp});
    },
    remove: (uid, key) => {
      // Delete project
      return getProjectRef(uid, key).remove().then(() => {
        // Delete project tasks
        db.ref(`todo-list/${uid}/lists/${key}`).remove();
      })
    },
    update: (uid, key, update) => {
      return getProjectRef(uid, key).update({...update})
    },
    fetchAll: uid => handleFetchAll(getAllProjectsRef(uid)),
    projectAdded: (uid, lastCreatedAt, action) => {
      const ref = getAllProjectsRef(uid);
      handleChildAdded(ref, lastCreatedAt, action);
    },
    projectRemoved: (uid, action) => {
      getAllProjectsRef(uid).on('child_removed', snap => {
        action(snap.key);
      })
    },
    projectChanged: (uid, action) => {
      getAllProjectsRef(uid).on('child_changed', snap => {
        action({[snap.key]: snap.val()})
      })
    }
  },
  tasks: {
    newOrder: (ref, action) => {
      ref.on('value', snap => {
        action(snap.val());
      });
    },
    fetchAll: ref => handleFetchAll(ref),
    taskAdded: (ref, lastCreatedAt, action) => handleChildAdded(ref, lastCreatedAt, action),
    taskRemoved: (ref, action) => {
      ref.on('child_removed', snap => {
        action(snap.key);
      });
    },
    taskChanged: (ref, action) => {
      ref.on('child_changed', snap => {
        action({[snap.key]: snap.val()})
      })
    },
    update: (ref, content) => ref.update(content),
    updateOrder: (ref, order) => {
      ref.set(order.join(' '));
    },
    remove: ref => {
      ref.remove();
    },
    add: ({uid, projectId, content, order, type}) => {
      const projectRef = db.ref(`todo-list/${uid}/lists/${projectId}`)
      const taskRef = projectRef.child('tasks').push();
      const orderRef = projectRef.child(`${type}Order`)
      const key = taskRef.key;
      const newOrder = [key, ...order];
      orderRef.set(newOrder.join(' '))

      if (type === 'pending') {
        return taskRef.set({...content, createdAt: timestamp})
      } else if (type === 'active') {
        return taskRef.set({...content, createdAt: timestamp, activeAt: timestamp})
      } else if (type === 'completed') {
        return taskRef.set({
          ...content,
          createdAt: timestamp,
          completedAt: timestamp,
          activeAt: timestamp})
      }
    }
  }

}
